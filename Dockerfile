FROM openjdk:16
EXPOSE 8080
ADD target/cra-data-portal.jar cra-data-portal.jar
ENTRYPOINT ["java", "-jar", "/cra-data-portal.jar"]