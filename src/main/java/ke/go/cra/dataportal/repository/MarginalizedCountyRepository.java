package ke.go.cra.dataportal.repository;


import ke.go.cra.dataportal.model.county.MarginalizedCounty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarginalizedCountyRepository extends JpaRepository<MarginalizedCounty, Integer> {
}
