package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.Municipality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MunicipalityRepository extends JpaRepository<Municipality, Integer> {

    List<Municipality> findAllByCountyCode(int countyCode);
}
