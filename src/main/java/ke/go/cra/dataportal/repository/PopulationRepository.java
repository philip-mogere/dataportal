package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.Population;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PopulationRepository extends JpaRepository<Population, Integer> {

    Population findPopulationByCountyCode(int countyCode);


    @Query(value="SELECT * FROM population where year=:year ORDER BY county_code", nativeQuery = true)
    List<Population> findAllByYear(int year);

}
