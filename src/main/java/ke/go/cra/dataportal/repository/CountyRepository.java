package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.County;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountyRepository extends CrudRepository<County, Integer> {

    @Query(value="SELECT * FROM county ORDER BY county_code", nativeQuery = true)
            List<County> findAllCounties();
}
