package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.CountyBudget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountyBudgetRepository extends JpaRepository<CountyBudget, Integer> {

    @Query(value="SELECT * FROM county_budget where year=:year ORDER BY county_code", nativeQuery = true)
    List<CountyBudget> findAllByYear(String year);

//    List<CountyBudget> findAllByCountyCode(int countyCode);
}
