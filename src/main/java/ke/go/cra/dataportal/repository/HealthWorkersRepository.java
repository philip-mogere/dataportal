package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.health.HealthWorkers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HealthWorkersRepository extends JpaRepository<HealthWorkers, Long> {

    List<HealthWorkers> findHealthWorkersByCountyCode(int countyCode);

    Optional<HealthWorkers> findHealthWorkerByCountyCode(int countyCode);

}
