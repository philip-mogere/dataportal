package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.OSR;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OSRRepository extends JpaRepository<OSR, Integer> {

//    convert rows to columns code
//    SELECT tableA_id,
//    MAX(CASE WHEN code ='code A' THEN 'yes' END) AS "code A",
//    MAX(CASE WHEN code ='code B' THEN 'yes' END) AS "code B",
//    MAX(CASE WHEN code ='code C' THEN 'yes' END) AS "code C"
//    FROM tableA
//    GROUP BY tableA_id;
}
