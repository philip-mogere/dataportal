package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.County;
import ke.go.cra.dataportal.model.county.CountyDisbursement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountyDisbursementRepository extends JpaRepository<CountyDisbursement, Integer> {

    @Query(value="SELECT * FROM county_disbursement ORDER BY county_county_code", nativeQuery = true)
    List<CountyDisbursement> findAll();

    List<CountyDisbursement> findAllByCounty(County county);
}
