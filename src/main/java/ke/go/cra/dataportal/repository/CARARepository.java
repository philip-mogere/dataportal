package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.CARA;
import ke.go.cra.dataportal.model.county.enums.Structure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CARARepository extends JpaRepository<CARA, Integer> {
    @Query(value="SELECT * FROM cara where year=:year ORDER BY county_code", nativeQuery = true)
    List<CARA> findAllByYear(int year);

    List<CARA> findAllByStructure(Structure structure);


}
