package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.CountyExecutive;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountyExecutiveRepository extends CrudRepository<CountyExecutive, Integer> {

//    CountyExecutive findCountyExecutivesByCountyExecutiveId(int id);


}
