package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountyAllocationsRepository extends JpaRepository<CountyAllocations, Long> {


    @Query(value="SELECT * FROM county_allocations where year=:year ORDER BY county_code", nativeQuery = true)
    List<CountyAllocations> findAllByYear(String year);

    List<CountyAllocations> findAllByCountyCode(int countyCode);

    List<EquitableShare> findByYear(String year);

    List<TotalLoansGrants> findTotalLoansGrantsByYear(String year);

//    List<TotalLoansGrants> findAllTotalLoansGrants();

//    List<TotalNationalGrants> findAllNationalGrants();

//    List<TotalRevenue> findTotalRevenue();

}
