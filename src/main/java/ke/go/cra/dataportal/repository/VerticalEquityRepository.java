package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.VerticalEquity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerticalEquityRepository extends JpaRepository<VerticalEquity, Integer> {

}
