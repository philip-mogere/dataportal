package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.MarginalizedThird;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarginalizedThirdRepository extends JpaRepository<MarginalizedThird, Long> {
}
