package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.health.HealthData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HealthDataRepository extends JpaRepository<HealthData, Long> {
    HealthData findAllByCountyCode(int countyCode);



}
