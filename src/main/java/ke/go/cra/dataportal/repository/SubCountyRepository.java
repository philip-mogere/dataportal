package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.SubCounty;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubCountyRepository extends CrudRepository<SubCounty, Integer> {

    List<SubCounty> findSubCountyByCountyCode(int countyCode);
}
