package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.CeilingExecutive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CeilingExecutiveRepository extends JpaRepository<CeilingExecutive, Integer> {
}
