package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.MarginalizedArea;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MarginalizedAreasRepository extends CrudRepository<MarginalizedArea, Integer> {

    List<MarginalizedArea> findAllByCountyCode(int countyCode);


}
