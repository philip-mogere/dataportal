package ke.go.cra.dataportal.repository.user;

import ke.go.cra.dataportal.model.user.PortalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<PortalUser, Long> {
    Optional<PortalUser> findByEmail(String email);

    Optional<PortalUser> findByUsernameOrEmail(String username, String email);

    List<PortalUser> findByIdIn(List<Long> userIds);

    Optional<PortalUser> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
