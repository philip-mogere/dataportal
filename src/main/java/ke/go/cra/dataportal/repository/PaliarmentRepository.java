package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.Paliarment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaliarmentRepository extends JpaRepository<Paliarment, Integer> {

    List<Paliarment> findAllByCountyCode(int countyCode);
}
