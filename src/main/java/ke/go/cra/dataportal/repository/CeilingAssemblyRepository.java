package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.county.CeilingAssembly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CeilingAssemblyRepository extends JpaRepository<CeilingAssembly, Integer> {
}
