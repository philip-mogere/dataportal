package ke.go.cra.dataportal.repository;

import ke.go.cra.dataportal.model.MarginalizationAddendum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarginalizationAddendumRepository extends JpaRepository<MarginalizationAddendum, Integer> {
}
