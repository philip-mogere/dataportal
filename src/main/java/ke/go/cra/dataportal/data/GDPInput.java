package ke.go.cra.dataportal.data;

import java.time.LocalDate;


public class GDPInput {

    private String Year;
    private String NominalGDPPrices;
    private String GDPGrowthPercentage;
    private String RealGDPPrices;

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getNominalGDPPrices() {
        return NominalGDPPrices;
    }

    public void setNominalGDPPrices(String nominalGDPPrices) {
        NominalGDPPrices = nominalGDPPrices;
    }

    public String getGDPGrowthPercentage() {
        return GDPGrowthPercentage;
    }

    public void setGDPGrowthPercentage(String GDPGrowthPercentage) {
        this.GDPGrowthPercentage = GDPGrowthPercentage;
    }

    public String getRealGDPPrices() {
        return RealGDPPrices;
    }

    public void setRealGDPPrices(String realGDPPrices) {
        RealGDPPrices = realGDPPrices;
    }
}
