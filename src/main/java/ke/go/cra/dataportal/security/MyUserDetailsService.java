package ke.go.cra.dataportal.security;

import ke.go.cra.dataportal.model.user.PortalUser;
import ke.go.cra.dataportal.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<PortalUser> userOpt = userRepository.findByUsername(userName);
        return userOpt.orElseThrow(() -> new UsernameNotFoundException("Invalid credentials"));
    }
}
