package ke.go.cra.dataportal.model.county;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CountyExecutive {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private int countyExecutiveId;
    private String Governor;
    @Column(columnDefinition = "TEXT")
    private String governorProfie;
    private String deputyGovernor;
    @Column(columnDefinition = "TEXT")
    private String deputyGovernorProfie;
    private String countySecretary;
    private String governorImage;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyExecutiveId")
    private List<CEC> cecs = new ArrayList<>();

    @JsonIgnore
    @OneToOne
    private County county;

    public CountyExecutive(){

    }

    public int getCountyExecutiveId() {
        return countyExecutiveId;
    }

    public void setCountyExecutiveId(int countyExecutiveId) {
        this.countyExecutiveId = countyExecutiveId;
    }

    public String getGovernor() {
        return Governor;
    }

    public void setGovernor(String governor) {
        Governor = governor;
    }

    public String getDeputyGovernor() {
        return deputyGovernor;
    }

    public void setDeputyGovernor(String deputyGovernor) {
        this.deputyGovernor = deputyGovernor;
    }

    public String getCountySecretary() {
        return countySecretary;
    }

    public void setCountySecretary(String countySecretary) {
        this.countySecretary = countySecretary;
    }

    public List<CEC> getCecs() {
        return cecs;
    }

    public void setCecs(List<CEC> cecs) {
        this.cecs = cecs;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public String getGovernorImage() {
        return governorImage;
    }

    public void setGovernorImage(String governorImage) {
        this.governorImage = governorImage;
    }

    public String getGovernorProfie() {
        return governorProfie;
    }

    public void setGovernorProfie(String governorProfie) {
        this.governorProfie = governorProfie;
    }

    public String getDeputyGovernorProfie() {
        return deputyGovernorProfie;
    }

    public void setDeputyGovernorProfie(String deputyGovernorProfie) {
        this.deputyGovernorProfie = deputyGovernorProfie;
    }
}
