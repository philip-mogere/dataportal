package ke.go.cra.dataportal.model.county;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CountyDisbursement {

    @Id
    private int id;
    private Long disbursement;
    private String period;
    private String countyName;


    @JsonIgnore
    @OneToOne
    private County county;

    public CountyDisbursement(){

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getDisbursement() {

        return disbursement;
    }

    public void setDisbursement(Long disbursement) {
        this.disbursement = disbursement;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }


}
