package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RegisteredVoters {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private int year;
    private long voters;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getVoters() {
        return voters;
    }

    public void setVoters(long voters) {
        this.voters = voters;
    }
}
