package ke.go.cra.dataportal.model.county;

public interface TotalRevenue {

    String getCountyName();
    Long getTotalRevenue();
    String getYear();
}
