package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CountyAllocations {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String countyName;
    private int countyCode;
    private long equitableShare;
    private long totalNationalGrants;
    private long totalNationalTransfers;
    private long totalLoansGrants;
    private long totalRevenue;
    private String year;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public long getEquitableShare() {
        return equitableShare;
    }

    public void setEquitableShare(long equitableShare) {
        this.equitableShare = equitableShare;
    }

    public long getTotalNationalGrants() {
        return totalNationalGrants;
    }

    public void setTotalNationalGrants(long totalNationalGrants) {
        this.totalNationalGrants = totalNationalGrants;
    }

    public long getTotalNationalTransfers() {
        return totalNationalTransfers;
    }

    public void setTotalNationalTransfers(long totalNationalTransfers) {
        this.totalNationalTransfers = totalNationalTransfers;
    }

    public long getTotalLoansGrants() {
        return totalLoansGrants;
    }

    public void setTotalLoansGrants(long totalLoansGrants) {
        this.totalLoansGrants = totalLoansGrants;
    }

    public long getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(long totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
