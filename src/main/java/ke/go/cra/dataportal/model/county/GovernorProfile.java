package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GovernorProfile {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private String name;
    private String profile;
    private int term;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
