package ke.go.cra.dataportal.model.response;

import ke.go.cra.dataportal.model.county.CountyBudget;

import javax.persistence.Column;
import javax.persistence.Id;

public class CountyBudgetResponse {
    @Id
    private int id;
    private long recurrent;
    private long development;
    @Column(nullable = true)
    private long equitableShare;
    @Column(nullable = true)
    private long ownRevenue;
    private String year;
    private String countyName;

    public CountyBudgetResponse(CountyBudget countyBudget){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getRecurrent() {
        return recurrent;
    }

    public void setRecurrent(long recurrent) {
        this.recurrent = recurrent;
    }

    public long getDevelopment() {
        return development;
    }

    public void setDevelopment(long development) {
        this.development = development;
    }

    public long getEquitableShare() {
        return equitableShare;
    }

    public void setEquitableShare(long equitableShare) {
        this.equitableShare = equitableShare;
    }

    public long getOwnRevenue() {
        return ownRevenue;
    }

    public void setOwnRevenue(long ownRevenue) {
        this.ownRevenue = ownRevenue;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}
