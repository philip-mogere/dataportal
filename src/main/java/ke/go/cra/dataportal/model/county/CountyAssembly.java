package ke.go.cra.dataportal.model.county;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CountyAssembly {

    @Id
    private int countyAssemblyId;
    private String speaker;
    private String deputySpeaker;
    private String clerk;
    private String majorityLeader;
    private String minorityLeader;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyAssemblyId")
    private List<MCA> mcas = new ArrayList<>();



    public CountyAssembly(){

    }

    public int getCountyAssemblyId() {
        return countyAssemblyId;
    }

    public void setCountyAssemblyId(int countyAssemblyId) {
        this.countyAssemblyId = countyAssemblyId;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getDeputySpeaker() {
        return deputySpeaker;
    }

    public void setDeputySpeaker(String deputySpeaker) {
        this.deputySpeaker = deputySpeaker;
    }

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public List<MCA> getMcas() {
        return mcas;
    }

    public void setMcas(List<MCA> mcas) {
        this.mcas = mcas;
    }

    public String getMajorityLeader() {
        return majorityLeader;
    }

    public void setMajorityLeader(String majorityLeader) {
        this.majorityLeader = majorityLeader;
    }

    public String getMinorityLeader() {
        return minorityLeader;
    }

    public void setMinorityLeader(String minorityLeader) {
        this.minorityLeader = minorityLeader;
    }
}
