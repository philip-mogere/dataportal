package ke.go.cra.dataportal.model.county;

public interface TotalLoansGrants {
    String getCountyName();
    Long getTotalLoansGrants();
    String getYear();
}
