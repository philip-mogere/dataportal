package ke.go.cra.dataportal.model.county;

public interface TotalNationalGrants {

    String getCountyName();
    Long getTotalNationalGrants();
    String getYear();

}
