package ke.go.cra.dataportal.model.response;

import ke.go.cra.dataportal.model.county.CEC;
import ke.go.cra.dataportal.model.county.CountyExecutive;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import javax.persistence.Column;
import javax.persistence.Lob;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CountyExecutiveResponse {

    private String Governor;

    @Column(columnDefinition = "TEXT")
    private String governorProfie;
    private String deputyGovernor;

    @Column(columnDefinition = "TEXT")
    private String gdeputyGovernorProfie;
    private String countySecretary;
    private byte[] imageBytes;
    private String governorImage;

    private List<CEC> cecs = new ArrayList<>();
    
    public CountyExecutiveResponse(CountyExecutive countyExecutive){
        this.Governor = countyExecutive.getGovernor();
        this.countySecretary = countyExecutive.getCountySecretary();
        this.imageBytes = getGovernorImagePath();
        this.cecs = countyExecutive.getCecs();
    }

    public String getGovernor() {
        return Governor;
    }

    public void setGovernor(String governor) {
        Governor = governor;
    }

    public String getDeputyGovernor() {
        return deputyGovernor;
    }

    public void setDeputyGovernor(String deputyGovernor) {
        this.deputyGovernor = deputyGovernor;
    }

    public String getCountySecretary() {
        return countySecretary;
    }

    public void setCountySecretary(String countySecretary) {
        this.countySecretary = countySecretary;
    }

    public String getGovernorImage() {
        return governorImage;
    }

    public void setGovernorImage(String governorImage) {
        this.governorImage = governorImage;
    }

    public List<CEC> getCecs() {
        return cecs;
    }

    public void setCecs(List<CEC> cecs) {
        this.cecs = cecs;
    }

    public byte[] getGovernorImagePath()  {
        ClassPathResource imageFile = new ClassPathResource("/" + governorImage + "/");
        try {
             imageBytes = StreamUtils.copyToByteArray(imageFile.getInputStream());
        }
        catch(IOException e){
            
        }
        return imageBytes;
    }
}
