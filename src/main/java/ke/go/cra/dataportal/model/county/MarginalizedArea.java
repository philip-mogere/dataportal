package ke.go.cra.dataportal.model.county;


import javax.persistence.*;

@Entity
public class MarginalizedArea {

    @Id
    @SequenceGenerator(
            name ="marginalized_area_generator",
            sequenceName = "marginalized_area_generator",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy= GenerationType.SEQUENCE,
            generator = "marginalized_area_generator"
    )
    private Long id;
    private String name;
    private int countyCode;
    private String countyName;
    private String constituency;
    private String ward;
    @Column(name="population_marginalized_area")
    private long population;
    @Column(name="net_secondary_attendance_rate")
    private float netSecondaryAttendanceRate;
    @Column(name="net_primary_attendance_rate")
    private float netPrimaryAttendance;
    @Column(name="households_access_to_electricity")
    private float householdAccessingElectricity;
    @Column(name="households_accessing_to_SafeWater")
    private float householdsAccessingSafeWater;
    @Column(name="households_accessing_to_sanitation")
    private float householdsAccessingImprovedSanitation;
    @Column(name="allocation_factor")
    private float allocationFactor;
    @Column(name="fund_allocated")
    private long fundAllocated;
    @Column(name="allocation_index")
    private float allocationIndex;
    @Column(name="fund_with_population")
    private float fundWithPopultaion;


    public MarginalizedArea(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public float getNetSecondaryAttendanceRate() {
        return netSecondaryAttendanceRate;
    }

    public void setNetSecondaryAttendanceRate(float netSecondaryAttendanceRate) {
        this.netSecondaryAttendanceRate = netSecondaryAttendanceRate;
    }

    public float getNetPrimaryAttendance() {
        return netPrimaryAttendance;
    }

    public void setNetPrimaryAttendance(float netPrimaryAttendance) {
        this.netPrimaryAttendance = netPrimaryAttendance;
    }

    public float getHouseholdAccessingElectricity() {
        return householdAccessingElectricity;
    }

    public void setHouseholdAccessingElectricity(float householdAccessingElectricity) {
        this.householdAccessingElectricity = householdAccessingElectricity;
    }

    public float getHouseholdsAccessingSafeWater() {
        return householdsAccessingSafeWater;
    }

    public void setHouseholdsAccessingSafeWaterForDrinking(float householdsAccessingSafeWater) {
        this.householdsAccessingSafeWater = householdsAccessingSafeWater;
    }

    public float getHouseholdsAccessingImprovedSanitation() {
        return householdsAccessingImprovedSanitation;
    }

    public void setHouseholdsAccessingImprovedSanitation(float householdsAccessingImprovedSanitation) {
        this.householdsAccessingImprovedSanitation = householdsAccessingImprovedSanitation;
    }

    public float getAllocationFactor() {
        return allocationFactor;
    }

    public void setAllocationFactor(float allocationFactor) {
        this.allocationFactor = allocationFactor;
    }

    public long getfundAllocated() {
        return fundAllocated;
    }

    public void setfundAllocated(long fundAllocated) {
        this.fundAllocated = fundAllocated;
    }

    public float getAllocationIndex() {
        return allocationIndex;
    }

    public void setAllocationIndex(float allocationIndex) {
        this.allocationIndex = allocationIndex;
    }

    public float getFundWithPopultaion() {
        return fundWithPopultaion;
    }

    public void setFundWithPopultaion(float fundWithPopultaion) {
        this.fundWithPopultaion = fundWithPopultaion;
    }
}
