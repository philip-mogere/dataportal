package ke.go.cra.dataportal.model.county;

import javax.persistence.*;

@Entity
public class MarginalizedThird {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private int numberOfConstituencies;
    private int numberOfSubLocations;
    private String countyName;
    private long totalFunding;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberOfConstituencies() {
        return numberOfConstituencies;
    }

    public void setNumberOfConstituencies(int numberOfConstituencies) {
        this.numberOfConstituencies = numberOfConstituencies;
    }

    public int getNumberOfSubLocations() {
        return numberOfSubLocations;
    }

    public void setNumberOfSubLocations(int numberOfSubLocations) {
        this.numberOfSubLocations = numberOfSubLocations;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public long getTotalFunding() {
        return totalFunding;
    }

    public void setTotalFunding(long totalFunding) {
        this.totalFunding = totalFunding;
    }
}
