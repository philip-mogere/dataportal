package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CountyGCP {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private int year;

    private Long AgricultureForestryAndFishing,
            MiningAndQuarrying,
            Manufacturing,
            ElectricitySupply,
            WaterSupplyWaste,
            Construction,
            WholesaleAndRetailTrade,
            TransportAndStorage,
            AccomodationAndFoodServices,
            InformationAndCommunication,
            FinancialAndInsuranceActivities,
            RealEstateActivities,
            ProfessionalTechnicalAndSupport,
            PublicAdministrationAndDefence,
            Education,
            HumanHealthAndSocialwork,
            OtherServiceActivities,
            Total;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Long getAgricultureForestryAndFishing() {
        return AgricultureForestryAndFishing;
    }

    public void setAgricultureForestryAndFishing(Long agricultureForestryAndFishing) {
        AgricultureForestryAndFishing = agricultureForestryAndFishing;
    }

    public Long getMiningAndQuarrying() {
        return MiningAndQuarrying;
    }

    public void setMiningAndQuarrying(Long miningAndQuarrying) {
        MiningAndQuarrying = miningAndQuarrying;
    }

    public Long getManufacturing() {
        return Manufacturing;
    }

    public void setManufacturing(Long manufacturing) {
        Manufacturing = manufacturing;
    }

    public Long getElectricitySupply() {
        return ElectricitySupply;
    }

    public void setElectricitySupply(Long electricitySupply) {
        ElectricitySupply = electricitySupply;
    }

    public Long getWaterSupplyWaste() {
        return WaterSupplyWaste;
    }

    public void setWaterSupplyWaste(Long waterSupplyWaste) {
        WaterSupplyWaste = waterSupplyWaste;
    }

    public Long getConstruction() {
        return Construction;
    }

    public void setConstruction(Long construction) {
        Construction = construction;
    }

    public Long getWholesaleAndRetailTrade() {
        return WholesaleAndRetailTrade;
    }

    public void setWholesaleAndRetailTrade(Long wholesaleAndRetailTrade) {
        WholesaleAndRetailTrade = wholesaleAndRetailTrade;
    }

    public Long getTransportAndStorage() {
        return TransportAndStorage;
    }

    public void setTransportAndStorage(Long transportAndStorage) {
        TransportAndStorage = transportAndStorage;
    }

    public Long getAccomodationAndFoodServices() {
        return AccomodationAndFoodServices;
    }

    public void setAccomodationAndFoodServices(Long accomodationAndFoodServices) {
        AccomodationAndFoodServices = accomodationAndFoodServices;
    }

    public Long getInformationAndCommunication() {
        return InformationAndCommunication;
    }

    public void setInformationAndCommunication(Long informationAndCommunication) {
        InformationAndCommunication = informationAndCommunication;
    }

    public Long getFinancialAndInsuranceActivities() {
        return FinancialAndInsuranceActivities;
    }

    public void setFinancialAndInsuranceActivities(Long financialAndInsuranceActivities) {
        FinancialAndInsuranceActivities = financialAndInsuranceActivities;
    }

    public Long getRealEstateActivities() {
        return RealEstateActivities;
    }

    public void setRealEstateActivities(Long realEstateActivities) {
        RealEstateActivities = realEstateActivities;
    }

    public Long getProfessionalTechnicalAndSupport() {
        return ProfessionalTechnicalAndSupport;
    }

    public void setProfessionalTechnicalAndSupport(Long professionalTechnicalAndSupport) {
        ProfessionalTechnicalAndSupport = professionalTechnicalAndSupport;
    }

    public Long getPublicAdministrationAndDefence() {
        return PublicAdministrationAndDefence;
    }

    public void setPublicAdministrationAndDefence(Long publicAdministrationAndDefence) {
        PublicAdministrationAndDefence = publicAdministrationAndDefence;
    }

    public Long getEducation() {
        return Education;
    }

    public void setEducation(Long education) {
        Education = education;
    }

    public Long getHumanHealthAndSocialwork() {
        return HumanHealthAndSocialwork;
    }

    public void setHumanHealthAndSocialwork(Long humanHealthAndSocialwork) {
        HumanHealthAndSocialwork = humanHealthAndSocialwork;
    }

    public Long getOtherServiceActivities() {
        return OtherServiceActivities;
    }

    public void setOtherServiceActivities(Long otherServiceActivities) {
        OtherServiceActivities = otherServiceActivities;
    }

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }
}
