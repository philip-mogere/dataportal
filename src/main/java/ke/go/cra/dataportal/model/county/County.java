package ke.go.cra.dataportal.model.county;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class County {
    @Id
    private int countyCode;
    private String countyName;
    private String countyHQ;
    private String governor;
    private int population;
//    private int landArea;
    private String regionalBloc;
    @Lob
    private byte[] coatOfArms;

    private String address;
    private String phone;
    private String website;
    private String offices;
    private String email;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private CountyExecutive countyExecutive;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private CountyAssembly countyAssembly;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private CountyDisbursement countyDisbursement;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private List<CountyBudget> countyBudget;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private List<Population> populationInfo;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private List<CARA> cara;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "countyCode")
    private List<CountyGCP> countyGCPs;




    public County() {
    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
    public String getCountyHQ() {
        return countyHQ;
    }

    public void setCountyHQ(String countyHQ) {
        this.countyHQ = countyHQ;
    }

    public String getGovernor() {
        return governor;
    }

    public void setGovernor(String governor) {
        this.governor = governor;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public CountyExecutive getCountyExecutive() {
        return countyExecutive;
    }

    public void setCountyExecutive(CountyExecutive countyExecutive) {
        this.countyExecutive = countyExecutive;
    }

    public CountyAssembly getCountyAssembly() {
        return countyAssembly;
    }

    public void setCountyAssembly(CountyAssembly countyAssembly) {
        this.countyAssembly = countyAssembly;
    }

    public List<CountyBudget> getCountyBudget() {
        return countyBudget;
    }

    public void setCountyBudget(List<CountyBudget> countyBudget) {
        this.countyBudget = countyBudget;
    }

    public List<Population> getPopulationInfo() {
        return populationInfo;
    }

    public void setPopulationInfo(List<Population> populationInfo) {
        this.populationInfo = populationInfo;
    }

    public CountyDisbursement getCountyDisbursement() {
        return countyDisbursement;
    }

    public void setCountyDisbursement(CountyDisbursement countyDisbursement) {
        this.countyDisbursement = countyDisbursement;
    }

//    public int getLandArea() {
//        return landArea;
//    }
//
//    public void setLandArea(int landArea) {
//        this.landArea = landArea;
//    }

    public String getRegionalBloc() {
        return regionalBloc;
    }

    public void setRegionalBloc(String regionalBloc) {
        this.regionalBloc = regionalBloc;
    }

    public List<CARA> getCara() {
        return cara;
    }

    public void setCara(List<CARA> cara) {
        this.cara = cara;
    }

    public byte[] getCoatOfArms() {
        return coatOfArms;
    }

    public void setCoatOfArms(byte[] coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOffices() {
        return offices;
    }

    public void setOffices(String offices) {
        this.offices = offices;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<CountyGCP> getCountyGCPs() {
        return countyGCPs;
    }

    public void setCountyGCPs(List<CountyGCP> countyGCPs) {
        this.countyGCPs = countyGCPs;
    }


}
