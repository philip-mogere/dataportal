package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CeilingAssembly {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    private String countyName;
    private long year_fourteen;
    private long year_fifteen;
    private long year_sixteen;
    private long year_seventeen;
    private long year_eighteen;
    private long year_nineteen;
    private long year_twenty;
    private long year_twentyOne;
    private int countyCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public long getYear_fourteen() {
        return year_fourteen;
    }

    public void setYear_fourteen(long year_fourteen) {
        this.year_fourteen = year_fourteen;
    }

    public long getYear_fifteen() {
        return year_fifteen;
    }

    public void setYear_fifteen(long year_fifteen) {
        this.year_fifteen = year_fifteen;
    }

    public long getYear_sixteen() {
        return year_sixteen;
    }

    public void setYear_sixteen(long year_sixteen) {
        this.year_sixteen = year_sixteen;
    }

    public long getYear_seventeen() {
        return year_seventeen;
    }

    public void setYear_seventeen(long year_seventeen) {
        this.year_seventeen = year_seventeen;
    }

    public long getYear_eighteen() {
        return year_eighteen;
    }

    public void setYear_eighteen(long year_eighteen) {
        this.year_eighteen = year_eighteen;
    }

    public long getYear_nineteen() {
        return year_nineteen;
    }

    public void setYear_nineteen(long year_nineteen) {
        this.year_nineteen = year_nineteen;
    }

    public long getYear_twenty() {
        return year_twenty;
    }

    public void setYear_twenty(long year_twenty) {
        this.year_twenty = year_twenty;
    }

    public long getYear_twentyOne() {
        return year_twentyOne;
    }

    public void setYear_twentyOne(long year_twentyOne) {
        this.year_twentyOne = year_twentyOne;
    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }
}
