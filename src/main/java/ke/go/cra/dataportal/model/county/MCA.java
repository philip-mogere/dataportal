package ke.go.cra.dataportal.model.county;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MCA {
    @Id
    private int id;
    private String name;
    private String ward;


    public MCA(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }
}
