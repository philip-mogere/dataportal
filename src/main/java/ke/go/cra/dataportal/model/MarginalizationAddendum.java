package ke.go.cra.dataportal.model;

import javax.persistence.*;

@Entity
public class MarginalizationAddendum {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;
    private String countyName;
    private String criteria;
    private long totalAllocation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public long getTotalAllocation() {
        return totalAllocation;
    }

    public void setTotalAllocation(long totalAllocation) {
        this.totalAllocation = totalAllocation;
    }
}
