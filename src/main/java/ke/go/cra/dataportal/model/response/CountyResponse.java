package ke.go.cra.dataportal.model.response;

import ke.go.cra.dataportal.model.county.County;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class CountyResponse {

    private int countyCode;
    private String countyName;
    private String countyHQ;
    private String governor;
    private int population;
    private String address;
    private String phone;
    private String website;
    private String offices;
    private String email;

    public CountyResponse(County county){
        this.countyCode = county.getCountyCode();
        this.countyHQ = county.getCountyHQ();
        this.countyName = county.getCountyName();
        this.address = county.getAddress();
        this.email = county.getEmail();
        this.governor = county.getGovernor();
        this.offices = county.getOffices();
        this.phone = county.getPhone();
        this.website = county.getWebsite();
    }
}
