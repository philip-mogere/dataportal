package ke.go.cra.dataportal.model.health;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class HealthData {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    private int countyCode;
    private String countyName;
    private int bedDensity;
    private double insuranceCoverage;
    private long outptientExpenditure;
    private long inpatientExpenditure;
    private double opdUtilizationRate;






    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public int getBedDensity() {
        return bedDensity;
    }

    public void setBedDensity(int bedDensity) {
        this.bedDensity = bedDensity;
    }

    public double getInsuranceCoverage() {
        return insuranceCoverage;
    }

    public void setInsuranceCoverage(double insuranceCoverage) {
        this.insuranceCoverage = insuranceCoverage;
    }

    public long getOutptientExpenditure() {
        return outptientExpenditure;
    }

    public void setOutptientExpenditure(long outptientExpenditure) {
        this.outptientExpenditure = outptientExpenditure;
    }

    public long getInpatientExpenditure() {
        return inpatientExpenditure;
    }

    public void setInpatientExpenditure(long inpatientExpenditure) {
        this.inpatientExpenditure = inpatientExpenditure;
    }

    public double getOpdUtilizationRate() {
        return opdUtilizationRate;
    }

    public void setOpdUtilizationRate(double opdUtilizationRate) {
        this.opdUtilizationRate = opdUtilizationRate;
    }
}
