package ke.go.cra.dataportal.model.national;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class NationalGDP {
    @Id
    private Long id;
    private int Year;
    private int NominalGDPPrices;
    private double GDPGrowthPercentage;
    private int RealGDPPrices;

    @Id
    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public int getNominalGDPPrices() {
        return NominalGDPPrices;
    }

    public void setNominalGDPPrices(int nominalGDPPrices) {
        NominalGDPPrices = nominalGDPPrices;
    }

    public double getGDPGrowthPercentage() {
        return GDPGrowthPercentage;
    }

    public void setGDPGrowthPercentage(double GDPGrowthPercentage) {
        this.GDPGrowthPercentage = GDPGrowthPercentage;
    }

    public int getRealGDPPrices() {
        return RealGDPPrices;
    }

    public void setRealGDPPrices(int realGDPPrices) {
        RealGDPPrices = realGDPPrices;
    }
}
