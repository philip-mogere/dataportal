package ke.go.cra.dataportal.model.user;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
