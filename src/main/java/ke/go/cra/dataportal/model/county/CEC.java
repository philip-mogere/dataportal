package ke.go.cra.dataportal.model.county;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CEC {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int CECId;
    private String CECName;
    private String Docket;


    public CEC(){

    }

    public int getCECId() {
        return CECId;
    }

    public void setCECId(int CECId) {
        this.CECId = CECId;
    }

    public String getCECName() {
        return CECName;
    }

    public void setCECName(String CECName) {
        this.CECName = CECName;
    }

    public String getDocket() {
        return Docket;
    }

    public void setDocket(String docket) {
        Docket = docket;
    }

}
