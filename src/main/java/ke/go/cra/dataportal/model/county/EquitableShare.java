package ke.go.cra.dataportal.model.county;

public interface EquitableShare {

    String getCountyName();
    Long getEquitableShare();
    String getYear();

}
