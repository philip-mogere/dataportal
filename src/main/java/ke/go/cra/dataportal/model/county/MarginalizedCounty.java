package ke.go.cra.dataportal.model.county;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MarginalizedCounty {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    private String county;
    private long countyPopulation;
    private float percentageShareOfCDI;
    private float percentageOfEqualShare;
    private float percentageCombinedShare;
    private long funding;
    private int policy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public long getCountyPopulation() {
        return countyPopulation;
    }

    public void setCountyPopulation(long countyPopulation) {
        this.countyPopulation = countyPopulation;
    }

    public float getPercentageShareOfCDI() {
        return percentageShareOfCDI;
    }

    public void setPercentageShareOfCDI(float percentageShareOfCDI) {
        this.percentageShareOfCDI = percentageShareOfCDI;
    }

    public float getPercentageOfEqualShare() {
        return percentageOfEqualShare;
    }

    public void setPercentageOfEqualShare(float percentageOfEqualShare) {
        this.percentageOfEqualShare = percentageOfEqualShare;
    }

    public float getPercentageCombinedShare() {
        return percentageCombinedShare;
    }

    public void setPercentageCombinedShare(float percentageCombinedShare) {
        this.percentageCombinedShare = percentageCombinedShare;
    }

    public long getFunding() {
        return funding;
    }

    public void setFunding(long funding) {
        this.funding = funding;
    }

    public int getPolicy() {
        return policy;
    }

    public void setPolicy(int policy) {
        this.policy = policy;
    }
}
