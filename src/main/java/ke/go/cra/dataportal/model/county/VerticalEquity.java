package ke.go.cra.dataportal.model.county;

import javax.persistence.*;

@Entity
public class VerticalEquity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;
    private String year;
    private String countyGovernment;
    private String nationalGovernment;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCountyGovernment() {
        return countyGovernment;
    }

    public void setCountyGovernment(String countyGovernment) {
        this.countyGovernment = countyGovernment;
    }

    public String getNationalGovernment() {
        return nationalGovernment;
    }

    public void setNationalGovernment(String nationalGovernment) {
        this.nationalGovernment = nationalGovernment;
    }
}
