package ke.go.cra.dataportal.model.health;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class HealthWorkers {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private int clinicalOfficers;
    private int communityHealthServiceStaff;
    private int dentalStaff;
    private int diagnosticsAndImaging;
    private int environmentalHealthStaff;
    private int healthAdministrativeStaff;
    private int healthInformationOnICT;
    private int HealthPromotionOfficers;
    private int hospitalMaintenanceStaff;
    private int HTSCounselor;
    private int medicalLaboratoryScientists;
    private int medicalOfficersAndSpecialists;
    private int medicalSocialWorkers;
    private int nursesAndSpecialistNurses;
    private int nutritionStaff;
    private int pharmacyStaff;
    private int plasterStaff;
    private int rehabilitativeStaff;
    private int supportStaff;
    private int countyCode;
    private String countyName;

    public int getClinicalOfficers() {
        return clinicalOfficers;
    }

    public void setClinicalOfficers(int clinicalOfficers) {
        this.clinicalOfficers = clinicalOfficers;
    }

    public int getCommunityHealthServiceStaff() {
        return communityHealthServiceStaff;
    }

    public void setCommunityHealthServiceStaff(int communityHealthServiceStaff) {
        this.communityHealthServiceStaff = communityHealthServiceStaff;
    }

    public int getDentalStaff() {
        return dentalStaff;
    }

    public void setDentalStaff(int dentalStaff) {
        this.dentalStaff = dentalStaff;
    }

    public int getDiagnosticsAndImaging() {
        return diagnosticsAndImaging;
    }

    public void setDiagnosticsAndImaging(int diagnosticsAndImaging) {
        this.diagnosticsAndImaging = diagnosticsAndImaging;
    }

    public int getEnvironmentalHealthStaff() {
        return environmentalHealthStaff;
    }

    public void setEnvironmentalHealthStaff(int environmentalHealthStaff) {
        this.environmentalHealthStaff = environmentalHealthStaff;
    }

    public int getHealthAdministrativeStaff() {
        return healthAdministrativeStaff;
    }

    public void setHealthAdministrativeStaff(int healthAdministrativeStaff) {
        this.healthAdministrativeStaff = healthAdministrativeStaff;
    }

    public int getHealthInformationOnICT() {
        return healthInformationOnICT;
    }

    public void setHealthInformationOnICT(int healthInformationOnICT) {
        this.healthInformationOnICT = healthInformationOnICT;
    }

    public int getHealthPromotionOfficers() {
        return HealthPromotionOfficers;
    }

    public void setHealthPromotionOfficers(int healthPromotionOfficers) {
        HealthPromotionOfficers = healthPromotionOfficers;
    }

    public int getHospitalMaintenanceStaff() {
        return hospitalMaintenanceStaff;
    }

    public void setHospitalMaintenanceStaff(int hospitalMaintenanceStaff) {
        this.hospitalMaintenanceStaff = hospitalMaintenanceStaff;
    }

    public int getHTSCounselor() {
        return HTSCounselor;
    }

    public void setHTSCounselor(int HTSCounselor) {
        this.HTSCounselor = HTSCounselor;
    }

    public int getMedicalLaboratoryScientists() {
        return medicalLaboratoryScientists;
    }

    public void setMedicalLaboratoryScientists(int medicalLaboratoryScientists) {
        this.medicalLaboratoryScientists = medicalLaboratoryScientists;
    }

    public int getMedicalOfficersAndSpecialists() {
        return medicalOfficersAndSpecialists;
    }

    public void setMedicalOfficersAndSpecialists(int medicalOfficersAndSpecialists) {
        this.medicalOfficersAndSpecialists = medicalOfficersAndSpecialists;
    }

    public int getMedicalSocialWorkers() {
        return medicalSocialWorkers;
    }

    public void setMedicalSocialWorkers(int medicalSocialWorkers) {
        this.medicalSocialWorkers = medicalSocialWorkers;
    }

    public int getNursesAndSpecialistNurses() {
        return nursesAndSpecialistNurses;
    }

    public void setNursesAndSpecialistNurses(int nursesAndSpecialistNurses) {
        this.nursesAndSpecialistNurses = nursesAndSpecialistNurses;
    }

    public int getNutritionStaff() {
        return nutritionStaff;
    }

    public void setNutritionStaff(int nutritionStaff) {
        this.nutritionStaff = nutritionStaff;
    }

    public int getPharmacyStaff() {
        return pharmacyStaff;
    }

    public void setPharmacyStaff(int pharmacyStaff) {
        this.pharmacyStaff = pharmacyStaff;
    }

    public int getPlasterStaff() {
        return plasterStaff;
    }

    public void setPlasterStaff(int plasterStaff) {
        this.plasterStaff = plasterStaff;
    }

    public int getRehabilitativeStaff() {
        return rehabilitativeStaff;
    }

    public void setRehabilitativeStaff(int rehabilitativeStaff) {
        this.rehabilitativeStaff = rehabilitativeStaff;
    }

    public int getSupportStaff() {
        return supportStaff;
    }

    public void setSupportStaff(int supportStaff) {
        this.supportStaff = supportStaff;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}
