package ke.go.cra.dataportal.model.national;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Kenya {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private long population;
    private long countyDisbursement;
    private long budget;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
