package ke.go.cra.dataportal.model.county;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Population {

    @Id
    private int id;
    private long rural;
    private long urban;
    private long female;
    private long male;
    @Column(nullable = true)
    private long intersex;
    @Column(nullable = true)
    private long ruralHouseholds;
    @Column(nullable = true)
    private long urbanHouseholds;
    @Column(nullable = true)
    private long totalPopulation;
    @Column(nullable = true)
    private long populationDensity;
    private int year;
    private int countyCode;
    private String countyName;

    public Population(){}

    public long getRural() {
        return rural;
    }

    public void setRural(long rural) {
        this.rural = rural;
    }

    public long getUrban() {
        return urban;
    }

    public void setUrban(long urban) {
        this.urban = urban;
    }

    public long getFemale() {
        return female;
    }

    public void setFemale(long female) {
        this.female = female;
    }

    public long getMale() {
        return male;
    }

    public void setMale(long male) {
        this.male = male;
    }

    public long getIntersex() {
        return intersex;
    }

    public void setIntersex(long intersex) {
        this.intersex = intersex;
    }

    public long getRuralHouseholds() {
        return ruralHouseholds;
    }

    public void setRuralHouseholds(long ruralHouseholds) {
        this.ruralHouseholds = ruralHouseholds;
    }

    public long getUrbanHouseholds() {
        return urbanHouseholds;
    }

    public void setUrbanHouseholds(long urbanHouseholds) {
        this.urbanHouseholds = urbanHouseholds;
    }

    public long getTotalPopulation() {
        return totalPopulation;
    }

    public void setTotalPopulation(long totalPopulation) {
        this.totalPopulation = totalPopulation;
    }

    public long getPopulationDensity() {return populationDensity;}

    public void setPopulationDensity(long populationDensity) { this.populationDensity = populationDensity;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//  id  public long getRegisteredVoters() {
//        return registeredVoters;
//    }
//
//    public void setRegisteredVoters(long registeredVoters) {
//        this.registeredVoters = registeredVoters;
//    }

    public int getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(int countyCode) {
        this.countyCode = countyCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}
