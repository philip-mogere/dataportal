package ke.go.cra.dataportal.model.county;

import javax.persistence.*;

@Entity
public class SubCounty {

    @Id
    @GeneratedValue
    (strategy=GenerationType.AUTO)
    private Integer id;
    private String name;
    public int countyCode;

    public SubCounty(){

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}