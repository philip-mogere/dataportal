package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.CeilingAssembly;
import ke.go.cra.dataportal.repository.CeilingAssemblyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CeilingAssemblyService {
    @Autowired
    private CeilingAssemblyRepository ceilingAssemblyRepository;


    public List<CeilingAssembly> getAllCeilings(){
        return ceilingAssemblyRepository.findAll();
    }
}
