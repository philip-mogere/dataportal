package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.health.HealthWorkers;
import ke.go.cra.dataportal.repository.HealthWorkersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HealthWorkersService {
    @Autowired
    private HealthWorkersRepository healthWorkersRepository;

    public List<HealthWorkers> getAllHealthWorkersData(){
        return healthWorkersRepository.findAll();
    }



    public List<HealthWorkers> getAllHealthWorkersByCounty(int countyCode) {
        return healthWorkersRepository.findHealthWorkersByCountyCode(countyCode);
    }

    public Optional<HealthWorkers> getHealthWorkerByCountyCode(int countyCode) {
        return healthWorkersRepository.findHealthWorkerByCountyCode(countyCode);
    }
}
