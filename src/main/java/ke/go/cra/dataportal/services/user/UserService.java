package ke.go.cra.dataportal.services.user;

import ke.go.cra.dataportal.model.user.PortalUser;
import ke.go.cra.dataportal.repository.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public Optional<PortalUser> getByUsername(String username){
        return userRepository.findByUsername(username);
    }

}
