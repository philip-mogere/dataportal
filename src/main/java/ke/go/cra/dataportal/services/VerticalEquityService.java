package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.VerticalEquity;
import ke.go.cra.dataportal.repository.VerticalEquityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VerticalEquityService {
    @Autowired
    private VerticalEquityRepository verticalEquityRepository;

    public List<VerticalEquity> getAllVerticalEquity(){
        return verticalEquityRepository.findAll();
    }
}
