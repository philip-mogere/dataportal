package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.MarginalizationAddendum;
import ke.go.cra.dataportal.repository.MarginalizationAddendumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarginalizationAddendumService {

    @Autowired
    private MarginalizationAddendumRepository marginalizationAddendumRepository;


    public List<MarginalizationAddendum> getAllMarginalized(){
        return marginalizationAddendumRepository.findAll();
    }

}
