package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.CountyDisbursement;
import ke.go.cra.dataportal.repository.CountyDisbursementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountyDisbursementService {

    @Autowired
    private CountyDisbursementRepository countyDisbursementRepository;

    public List<CountyDisbursement> getAllCountyDisbursements(){
        return countyDisbursementRepository.findAll();
    }
}
