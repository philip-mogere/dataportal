package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.MarginalizedThird;
import ke.go.cra.dataportal.repository.MarginalizedThirdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarginalizedThirdService {
    @Autowired
    private MarginalizedThirdRepository marginalizedThirdRepository;

    public List<MarginalizedThird> getAllMarginalizedCounties(){
        return marginalizedThirdRepository.findAll();
    }

}
