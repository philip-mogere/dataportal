package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.CARA;
import ke.go.cra.dataportal.model.county.enums.Structure;
import ke.go.cra.dataportal.repository.CARARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CARAService {

    @Autowired
    private CARARepository caraRepository;

    public List<CARA> getAllCeilings(){
        return caraRepository.findAll();
    }

    public List<CARA> getAllCeilingsByYear(int year){
        return caraRepository.findAllByYear(year);
    }

    public List<CARA> getAllCeilingsByStructure(Structure structure){
        return caraRepository.findAllByStructure(structure);
    }

}
