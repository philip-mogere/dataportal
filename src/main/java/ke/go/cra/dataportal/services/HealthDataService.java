package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.health.HealthData;
import ke.go.cra.dataportal.repository.HealthDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HealthDataService {
    @Autowired
    private HealthDataRepository healthDataRepository;

    public List<HealthData> getAllHealthData(){
        return healthDataRepository.findAll();
    }

    public HealthData getHealthDataByCounty(int countyCode) {
        return healthDataRepository.findAllByCountyCode(countyCode);
    }
}
