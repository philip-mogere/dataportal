package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.*;
import ke.go.cra.dataportal.repository.CountyAllocationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class CountyAllocationsService {

    @Autowired
    private CountyAllocationsRepository countyAllocationsRepository;


    public List<CountyAllocations> getAllAllocations() {
        return countyAllocationsRepository.findAll();
    }

    public List<CountyAllocations> getAllByCountyCode(int countyCode){
        return countyAllocationsRepository.findAllByCountyCode(countyCode);
    }

    public List<CountyAllocations> getAllAllocationsByYear(String year) {
        return countyAllocationsRepository.findAllByYear(year);
    }

    public List<EquitableShare> getAllEquitableShare(String year) {
        return countyAllocationsRepository.findByYear(year);
    }

    public List<TotalLoansGrants> getAllTotalLoansGrantsByYear(String year){
        return countyAllocationsRepository.findTotalLoansGrantsByYear(year);
    }
//
//    public List<TotalLoansGrants> getAllTotalLoansGrants(){
//        return countyAllocationsRepository.findTotalLoansGrants();
//    }
//
//    public List<TotalNationalGrants> getAllNationalTransfers(){
//        return countyAllocationsRepository.findAllNationalGrants();
//    }
//
//    public List<TotalRevenue> getAllTotalRevenue(){
//        return countyAllocationsRepository.findAllTotalRevenue();
    }

