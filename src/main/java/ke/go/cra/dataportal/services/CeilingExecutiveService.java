package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.CeilingExecutive;
import ke.go.cra.dataportal.repository.CeilingExecutiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CeilingExecutiveService {
    @Autowired
    private CeilingExecutiveRepository ceilingExecutiveRepository;

    public List<CeilingExecutive> getAllCeilings(){
        return ceilingExecutiveRepository.findAll();
    }
}
