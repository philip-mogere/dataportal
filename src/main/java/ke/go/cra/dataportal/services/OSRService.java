package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.OSR;
import ke.go.cra.dataportal.repository.OSRRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OSRService {
    @Autowired
    private OSRRepository osrRepository;

    public List<OSR> getAll(){
        return osrRepository.findAll();
    }
}
