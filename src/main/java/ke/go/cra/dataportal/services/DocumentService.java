package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.document.Document;
import ke.go.cra.dataportal.repository.DocumentRepository;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    public Document storeDocument(MultipartFile document) throws IOException {
        String documentName = StringUtils.cleanPath(document.getOriginalFilename());
        try {
            Document newDocument = new Document(documentName, document.getContentType(), document.getBytes());
            return documentRepository.save(newDocument);
        }
        catch(IOException exception){
            throw new FileUploadException("Could not store file" + documentName + ". Please try again", exception);
        }
    }

    public Document getFile(Long documentId) throws FileNotFoundException {
        return documentRepository.findById(documentId).orElseThrow(() -> new FileNotFoundException("File not found with id"+documentId));
    }
}
