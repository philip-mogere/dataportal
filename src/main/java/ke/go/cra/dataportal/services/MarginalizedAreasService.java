package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.MarginalizedArea;
import ke.go.cra.dataportal.repository.MarginalizedAreasRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MarginalizedAreasService {

    private final MarginalizedAreasRepository marginalizedAreasRepository;

    public MarginalizedAreasService(MarginalizedAreasRepository marginalizedAreasRepository) {
        this.marginalizedAreasRepository = marginalizedAreasRepository;
    }

    public List<MarginalizedArea> getAllMarginalizedAreas(){
        return StreamSupport
                .stream(marginalizedAreasRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<MarginalizedArea> getAllByCountyCode(int countyCode){
        return marginalizedAreasRepository.findAllByCountyCode(countyCode);
    }

}
