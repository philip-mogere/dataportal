package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.County;
import ke.go.cra.dataportal.model.county.CountyAssembly;
import ke.go.cra.dataportal.model.county.CountyBudget;
import ke.go.cra.dataportal.model.county.Population;
import ke.go.cra.dataportal.model.response.CountyResponse;
import ke.go.cra.dataportal.repository.CountyAssemblyRepository;
import ke.go.cra.dataportal.repository.CountyBudgetRepository;
import ke.go.cra.dataportal.repository.CountyRepository;
import ke.go.cra.dataportal.repository.PopulationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CountyService {

    private final CountyRepository countyRepository;
    private final CountyBudgetRepository countyBudgetRepository;
    private final CountyAssemblyRepository countyAssemblyRepository;
    private final PopulationRepository populationRepository;


    @Autowired
    public CountyService(CountyRepository countyRepository,
                         CountyBudgetRepository countyBudgetRepository,
                         CountyAssemblyRepository countyAssemblyRepository,
                         PopulationRepository populationRepository) {
        this.countyRepository = countyRepository;
        this.countyBudgetRepository = countyBudgetRepository;
        this.countyAssemblyRepository = countyAssemblyRepository;
        this.populationRepository = populationRepository;
    }

    public List<County> findAll() {
        return StreamSupport.stream(countyRepository.findAllCounties().spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<CountyResponse> findAllCounties(){
        List<County> counties= findAll();
        return counties.stream().map(county -> new CountyResponse(county)).collect(Collectors.toList());
    }

    public County findByCountyCode(int countyCode) {
        return countyRepository.findById(countyCode).orElseThrow(()->
                new RuntimeException());
    }

    public CountyResponse findCountyByCountyCode(int countyCode) {
        County county = countyRepository.findById(countyCode).orElseThrow(()->
                new RuntimeException());
        return new CountyResponse(county);
    }

    public List<CountyBudget> getCountyBudgetsByYear(String year){
        return countyBudgetRepository.findAllByYear(year);
    }

    public List<CountyAssembly> findAllAssemblies(){
        return new ArrayList<>(countyAssemblyRepository.findAll());
    }

    public CountyAssembly findByAssemblyId(int countyExecutiveCode) {
        return countyAssemblyRepository.findById(countyExecutiveCode).orElseThrow(RuntimeException::new);
    }


    public List<CountyBudget> findAllCountyBudgets(){
        return new ArrayList<>(countyBudgetRepository.findAll());
    }

    public List<Population> getAllPopulation(){
        return new ArrayList<>(populationRepository.findAll());
    }

    public CountyBudget findByCountyBudgetId(int countyExecutiveCode) {
        return countyBudgetRepository.findById(countyExecutiveCode).orElseThrow(RuntimeException::new);
    }

    public Population findPopulationById(int countyCode){
        return populationRepository.getById(countyCode);
    }


//    public List<CountyBudget> getAllBudgetsByCounty(int countyCode) {
//        return countyBudgetRepository.findAllByCountyCode(countyCode);
//    }
}
