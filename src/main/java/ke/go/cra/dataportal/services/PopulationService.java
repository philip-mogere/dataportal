package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.Population;
import ke.go.cra.dataportal.repository.PopulationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PopulationService {

    private final PopulationRepository populationRepository;

    public PopulationService(PopulationRepository populationRepository) {
        this.populationRepository = populationRepository;
    }


    public List<Population> getPopulation(){
       return populationRepository.findAll();
    }


    public Population getCountyPopulation(int countyCode) {
        return populationRepository.findPopulationByCountyCode(countyCode);
    }


    public List<Population> getPopulationByYear(int year) {
        return populationRepository.findAllByYear(year);
    }
}
