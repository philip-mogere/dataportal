package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.CountyExecutive;
import ke.go.cra.dataportal.model.response.CountyExecutiveResponse;
import ke.go.cra.dataportal.model.response.CountyResponse;
import ke.go.cra.dataportal.repository.CountyExecutiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CountyExecutiveService {

    private CountyExecutiveRepository countyExecutiveRepository;

    @Autowired
    public CountyExecutiveService(CountyExecutiveRepository countyExecutiveRepository){
        this.countyExecutiveRepository=countyExecutiveRepository;
    }

    public List<CountyExecutive> findAll(){
        return StreamSupport.stream(countyExecutiveRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public CountyExecutive findById(int countyExecutiveCode) {
        return countyExecutiveRepository.findById(countyExecutiveCode).orElseThrow(()->
                new RuntimeException());
    }

    public List<CountyExecutiveResponse> findAllExecutive(){
        List<CountyExecutive> countyExecutives = findAll();
        return countyExecutives.stream().map(countyExecutive -> new CountyExecutiveResponse(countyExecutive)).collect(Collectors.toList());

    }
}
