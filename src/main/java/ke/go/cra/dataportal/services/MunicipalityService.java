package ke.go.cra.dataportal.services;
import ke.go.cra.dataportal.model.county.Municipality;
import ke.go.cra.dataportal.repository.MunicipalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipalityService {
    @Autowired
    private MunicipalityRepository municipalityRepository;
    public List<Municipality> getMunicipalityByCountyCode(int countyCode){
        return municipalityRepository.findAllByCountyCode(countyCode);
    }

    public List<Municipality> getAllMunicipalities(){
        return municipalityRepository.findAll();
    }
}
