package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.SubCounty;
import ke.go.cra.dataportal.repository.SubCountyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SubCountyService {

    private final SubCountyRepository subCountyRepository;

    public SubCountyService(SubCountyRepository subCountyRepository){
        this.subCountyRepository = subCountyRepository;
    }

    public List<SubCounty> getAllSubCounties(){
        return StreamSupport
                .stream(subCountyRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public SubCounty getByCountyCode(int id){
        return subCountyRepository.findById(id).orElseThrow(() ->
                new RuntimeException());
    }

    public List<SubCounty> getSubcountiesByCountyCode(int countyCode){
        return subCountyRepository.findSubCountyByCountyCode(countyCode);
    }
}
