package ke.go.cra.dataportal.services;

import ke.go.cra.dataportal.model.county.Paliarment;
import ke.go.cra.dataportal.repository.PaliarmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaliarmentService {

    @Autowired
    private PaliarmentRepository paliarmentRepository;

    public List<Paliarment> getAllConstituencies(){
        return paliarmentRepository.findAll();
    }

    public List<Paliarment> getAllConstituenciesByCountyCode(int countyCode){
        return paliarmentRepository.findAllByCountyCode(countyCode);
    }
}
