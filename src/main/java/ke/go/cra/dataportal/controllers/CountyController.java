package ke.go.cra.dataportal.controllers;

import ke.go.cra.dataportal.model.MarginalizationAddendum;
import ke.go.cra.dataportal.model.county.*;
import ke.go.cra.dataportal.model.county.enums.Structure;
import ke.go.cra.dataportal.model.health.HealthData;
import ke.go.cra.dataportal.model.health.HealthWorkers;
import ke.go.cra.dataportal.model.response.CountyResponse;
import ke.go.cra.dataportal.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping("/county")
class CountyController {

    private final CountyService service;
    private final PopulationService populationService;
    private final CountyExecutiveService executiveService;
    private final MarginalizedAreasService marginalizedAreasService;
    private final HealthWorkersService healthWorkersService;
    private final HealthDataService healthDataService;
    private final SubCountyService subCountyService;
    private final PaliarmentService paliarmentService;
    private final OSRService osrService;
    private final MunicipalityService municipalityService;
    private final MarginalizedCountyService marginalizedCountyService;
    private final CountyDisbursementService countyDisbursementService;
    private final CARAService caraService;
    private final CountyAllocationsService countyAllocationsService;
    private final VerticalEquityService verticalEquityService;
    private final MarginalizedThirdService marginalizedThirdService;
    private final MarginalizationAddendumService marginalizedAddendumService;
    private final CeilingAssemblyService ceilingAssemblyService;
    private final CeilingExecutiveService ceilingExecutiveService;



    @Autowired
    public CountyController(CountyService service, PopulationService populationService,
                            CountyExecutiveService executiveService, MarginalizedAreasService marginalizedAreasService,
                            HealthWorkersService healthWorkersService, HealthDataService healthDataService,
                            SubCountyService subCountyService, PaliarmentService paliarmentService, OSRService osrService,
                            MunicipalityService municipalityService, MarginalizedCountyService marginalizedCountyService,
                            CountyDisbursementService countyDisbursementService, CARAService caraService,
                            CountyAllocationsService countyAllocationsService, VerticalEquityService verticalEquityService,
                            MarginalizedThirdService marginalizedThirdService, MarginalizationAddendumService marginalizedAddendumService, CeilingAssemblyService ceilingAssemblyService, CeilingExecutiveService ceilingExecutiveService) {
        this.service = service;
        this.populationService = populationService;
        this.executiveService=executiveService;
        this.marginalizedAreasService = marginalizedAreasService;
        this.healthWorkersService = healthWorkersService;
        this.healthDataService = healthDataService;
        this.subCountyService = subCountyService;
        this.paliarmentService = paliarmentService;
        this.osrService = osrService;
        this.municipalityService = municipalityService;
        this.marginalizedCountyService = marginalizedCountyService;
        this.countyDisbursementService = countyDisbursementService;
        this.caraService = caraService;
        this.countyAllocationsService = countyAllocationsService;
        this.verticalEquityService = verticalEquityService;
        this.marginalizedThirdService = marginalizedThirdService;
        this.marginalizedAddendumService = marginalizedAddendumService;
        this.ceilingAssemblyService = ceilingAssemblyService;
        this.ceilingExecutiveService = ceilingExecutiveService;
    }

    @GetMapping("extended")
    public ResponseEntity<?> findAllCounties() {
        List<County> countyList = service.findAll();
        return ResponseEntity.ok(countyList);
    }

    @GetMapping("")
    public ResponseEntity<?> findAllCountiesList(){
        List<CountyResponse> countyResponseList = service.findAllCounties();
        return ResponseEntity.ok(countyResponseList);
    }

    @GetMapping(value = "/extended/{countyCode}")
    public County findById(@PathVariable("countyCode") int countyCode) {
        return service.findByCountyCode(countyCode);
    }

    @GetMapping("/{countyCode}/subcounty")
    public ResponseEntity<?> getSubcountiesByCountyCode(@PathVariable("countyCode") int countyCode){
        List<SubCounty> subCountyList = subCountyService.getSubcountiesByCountyCode(countyCode);
        return ResponseEntity.ok(subCountyList);
    }

    @GetMapping(value = "/{countyCode}")
    public ResponseEntity<?> findByCountyCode(@PathVariable("countyCode") int countyCode) {
        County county = service.findByCountyCode(countyCode);
        CountyResponse countyResponse = new CountyResponse(county);
        return ResponseEntity.ok(countyResponse);
    }


    @GetMapping("/{countyCode}/executive")
    public ResponseEntity<?> getExecutive(@PathVariable("countyCode") int countyCode){
        CountyExecutive countyExecutive = executiveService.findById(countyCode);
        return ResponseEntity.ok(countyExecutive);
    }

    @GetMapping("/{countyCode}/budget")
    public ResponseEntity<?> getBudget(@PathVariable("countyCode") int countyCode){
        CountyBudget countyBudget = service.findByCountyBudgetId(countyCode);
        return ResponseEntity.ok(countyBudget);
    }

    @GetMapping("/{countyCode}/assembly")
    public ResponseEntity<?> getAssemblyById(@PathVariable("countyCode") int countyCode){
        CountyAssembly countyAssembly = service.findByAssemblyId(countyCode);
        return ResponseEntity.ok(countyAssembly);
    }

    @GetMapping("population")
    public ResponseEntity<?> getPopulation(){
        List<Population> populationList =  populationService.getPopulation();
        return ResponseEntity.ok(populationList);
    }

    @GetMapping("populations")
    public ResponseEntity<?> getPopulationByYear(@RequestParam(name="year") int year){
        List<Population> populationList = populationService.getPopulationByYear(year);
        return ResponseEntity.ok(populationList);
    }
    @GetMapping("/{countyCode}/marginalized")
    public ResponseEntity<?> getMarginalizedAreaOfCounty(@PathVariable("countyCode") int countyCode){
        List<MarginalizedArea> marginalizedAreaList =  marginalizedAreasService.getAllByCountyCode(countyCode);
        return ResponseEntity.ok(marginalizedAreaList);
    }

    @GetMapping("marginalized")
    public ResponseEntity<?> getAllMarginalizedAreas(){
        List<MarginalizedArea> marginalizedAreaList = marginalizedAreasService.getAllMarginalizedAreas();
        return ResponseEntity.ok(marginalizedAreaList);
    }

    @GetMapping("budget")
    public ResponseEntity<?> getCountyBudgetsByYear(@RequestParam("year") String year){
        List<CountyBudget> countyBudgets = service.getCountyBudgetsByYear(year);
        return ResponseEntity.ok(countyBudgets);
    }

    @GetMapping("budgets")
    public ResponseEntity<?> getAllBudget(){
        List<CountyBudget> countyBudgets = service.findAllCountyBudgets();
        return ResponseEntity.ok(countyBudgets);
    }


    @GetMapping("healthStaff")
    public List<HealthWorkers> getAllHealthWorkers(){
        return healthWorkersService.getAllHealthWorkersData();
    }


    @GetMapping("{countyCode}/healthStaff")
    public ResponseEntity<?> getHealthWorkersByCountyCode(@PathVariable("countyCode") int countyCode){
        Optional<HealthWorkers> healthWorker = healthWorkersService.getHealthWorkerByCountyCode(countyCode);
        return ResponseEntity.ok(healthWorker);
    }

    @GetMapping("healthData")
    public ResponseEntity<?> getAllHealthData(){
        List<HealthData> healthData = healthDataService.getAllHealthData();
        return ResponseEntity.ok(healthData);
    }

    @GetMapping("{countyCode}/healthData")
    public ResponseEntity<?> getHealthDataByCounty(@PathVariable("countyCode") int countyCode){
        HealthData healthData = healthDataService.getHealthDataByCounty(countyCode);
        return ResponseEntity.ok(healthData);
    }


    @GetMapping("paliarment")
    public ResponseEntity<?> getAllConstituencies(){
        List<Paliarment> paliarmentList = paliarmentService.getAllConstituencies();
        return ResponseEntity.ok(paliarmentList);
    }

    @GetMapping("{countyCode}/paliarment")
    public ResponseEntity<?> getAllConstituenciesByCountyCode(@PathVariable("countyCode") int countyCode){
        List<Paliarment> paliarmentList = paliarmentService.getAllConstituenciesByCountyCode(countyCode);
        return ResponseEntity.ok(paliarmentList);
    }

    @GetMapping("osr")
    public ResponseEntity<?> getAllOsr(){
        List<OSR> osrList = osrService.getAll();
        return ResponseEntity.ok(osrList);
    }

    @GetMapping("{countyCode}/municipality")
    public ResponseEntity<?> getMunicipalityByCountyCode(@PathVariable("countyCode") int countyCode){
        List<Municipality> municipalities = municipalityService.getMunicipalityByCountyCode(countyCode);
        return ResponseEntity.ok(municipalities);
    }

    @GetMapping("municipality")
    public  ResponseEntity<?> getAllMunicipalities(){
        List<Municipality> municipalities = municipalityService.getAllMunicipalities();
        return ResponseEntity.ok(municipalities);
    }

    @GetMapping("marginalizedCounty")
    public ResponseEntity<?> getAllMarginalizedCounties(){
        List<MarginalizedCounty> marginalizedCounties = marginalizedCountyService.getAllMarginalizedCounties();
        return ResponseEntity.ok(marginalizedCounties);
    }

    @GetMapping("disbursements")
    public ResponseEntity<?> getAllCountyDisbursements(){
        List<CountyDisbursement> countyDisbursementList = countyDisbursementService.getAllCountyDisbursements();
        return ResponseEntity.ok(countyDisbursementList);
    }

    @GetMapping("cara")
    public ResponseEntity<?> getAllCeilings(){
        List<CARA> caraList = caraService.getAllCeilings();
        return ResponseEntity.ok(caraList);
    }

    @GetMapping("caras")
    public ResponseEntity<?> getAllCeilingsByYear(@RequestParam int year){
        List<CARA> caraList = caraService.getAllCeilingsByYear(year);
        return ResponseEntity.ok(caraList);
    }

    @GetMapping("caralist")
    public ResponseEntity<?> getAllCeilingsByStructure(@RequestParam Structure structure){
        List<CARA> caraList = caraService.getAllCeilingsByStructure(structure);
        return ResponseEntity.ok(caraList);
    }

    @GetMapping("/allocations")
    public ResponseEntity<?> getAllAllocations(){
        List<CountyAllocations> countyAllocationsList = countyAllocationsService.getAllAllocations();
        return ResponseEntity.ok(countyAllocationsList);
    }

    @GetMapping("/allocation")
    public ResponseEntity<?> getAllAllocationsByYear(@RequestParam String year){
        List<CountyAllocations> countyAllocationsList =  countyAllocationsService.getAllAllocationsByYear(year);
        return ResponseEntity.ok(countyAllocationsList);
    }

    @GetMapping("/equitable")
    public ResponseEntity<?> getAllEquitableShareAllocations(@RequestParam String year){
        List<EquitableShare> countyAllocationsList =  countyAllocationsService.getAllEquitableShare(year);
        return ResponseEntity.ok(countyAllocationsList);
    }

    @GetMapping("/totalloans")
    public ResponseEntity<?> getAllTotalLoansGrants(@RequestParam String year){
        List<TotalLoansGrants> countyAllocationsList =  countyAllocationsService.getAllTotalLoansGrantsByYear(year);
        return ResponseEntity.ok(countyAllocationsList);
    }

    @GetMapping("{countyCode}/allocation")
    public ResponseEntity<?> getAllAllocationsByCountyCode(@PathVariable("countyCode") int countyCode){
        List<CountyAllocations> countyAllocationsList = countyAllocationsService.getAllByCountyCode(countyCode);
        return ResponseEntity.ok(countyAllocationsList);
    }

    @GetMapping("/vertical")
    public ResponseEntity<?> getAllVerticalEquity(){
        List<VerticalEquity> verticalEquityList = verticalEquityService.getAllVerticalEquity();
        return ResponseEntity.ok(verticalEquityList);
    }

//    @GetMapping("/totalloansgrants")
//    public ResponseEntity<?> getAllTotalLoansGrants(){
//         List<TotalLoansGrants> totalLoansGrantsList = countyAllocationsService.getAllTotalLoansGrants();
//        return ResponseEntity.ok(totalLoansGrantsList);
//    }
//
//    @GetMapping("/totalnationalgrants")
//    public ResponseEntity<?> getAllNationalTransfers(){
//        List<TotalNationalGrants> nationalTransfersList = countyAllocationsService.getAllNationalTransfers();
//        return ResponseEntity.ok(nationalTransfersList);
//    }
//
//    @GetMapping("/totalrevenue")
//    public ResponseEntity<?>  getAllTotalRevenue(){
//        List<TotalRevenue> totalRevenueList =countyAllocationsService.getAllTotalRevenue();
//        return ResponseEntity.ok(totalRevenueList);
//    }
@   GetMapping("/marginalization-third")
    public ResponseEntity<?> getAllMarginalizationThirdPolicy(){
        List<MarginalizedThird> marginalizedThirdList = marginalizedThirdService.getAllMarginalizedCounties();
        return ResponseEntity.ok(marginalizedThirdList);
    }

    @GetMapping("/marginalization-addendum")
    public ResponseEntity<?> getAllMarginalizationAddendum(){
        List<MarginalizationAddendum> marginalizedAddendum = marginalizedAddendumService.getAllMarginalized();
        return ResponseEntity.ok(marginalizedAddendum);
    }

    @GetMapping("/executive-ceiling")
    public ResponseEntity<?> getAllExecutiveCeilings(){
        List<CeilingExecutive> ceilingExecutiveList = ceilingExecutiveService.getAllCeilings();
        return ResponseEntity.ok(ceilingExecutiveList);
    }

    @GetMapping("/assembly-ceiling")
    public ResponseEntity<?> getAllAssemblyCeilings(){
        List<CeilingAssembly> ceilingAssemblyList = ceilingAssemblyService.getAllCeilings();
        return ResponseEntity.ok(ceilingAssemblyList);
    }
}


