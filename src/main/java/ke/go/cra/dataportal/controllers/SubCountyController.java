package ke.go.cra.dataportal.controllers;

import ke.go.cra.dataportal.model.county.SubCounty;
import ke.go.cra.dataportal.services.SubCountyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/subcounty")
public class SubCountyController {

    private SubCountyService subCountyService;

    public SubCountyController(SubCountyService subCountyService){
        this.subCountyService = subCountyService;
    }

    @GetMapping("/")
    public List<SubCounty> getAllSubCounties(){
        return subCountyService.getAllSubCounties();
    }

//    @GetMapping(value = "/{id}")
//    public SubCounty getById(@PathVariable("id") int id){
//        return subCountyService.getById(id);
//    }
}
