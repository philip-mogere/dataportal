package ke.go.cra.dataportal.controllers;

import ke.go.cra.dataportal.model.county.CountyExecutive;
import ke.go.cra.dataportal.services.CountyExecutiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/countyexec")
public class CountyExecutiveController {

    private CountyExecutiveService countyExecutiveService;

    @Autowired
    public CountyExecutiveController(CountyExecutiveService countyExecutiveService){
        this.countyExecutiveService=countyExecutiveService;
    }

    @GetMapping("")
    public List<CountyExecutive> findAll(){
        return countyExecutiveService.findAll();
    }

    @GetMapping(value = "/{countyCode}")
    public CountyExecutive findCountyExecutiveByCountyCode(@PathVariable("countyCode") int countyCode) {
        return countyExecutiveService.findById(countyCode);
    }



}
