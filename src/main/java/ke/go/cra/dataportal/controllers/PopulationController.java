package ke.go.cra.dataportal.controllers;

import ke.go.cra.dataportal.model.county.Population;
import ke.go.cra.dataportal.services.PopulationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/population")
public class PopulationController {


    private PopulationService populationService;

    public PopulationController(PopulationService populationService) {
        this.populationService = populationService;
    }

    @GetMapping("")
    public List<Population> getPopulation(){
        return populationService.getPopulation();
    }

    @GetMapping("/{countyCode}")
    public Population getCountyPopulation(@RequestParam("countyCode") int countyCode) {
        return populationService.getCountyPopulation(countyCode);
    }



}
